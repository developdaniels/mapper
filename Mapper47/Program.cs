﻿using Mapper.Interfaces;
using Mapper.Mapper;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Mapper47
{
    class Program
    {
        public static int minLimit = 0;
        public static int maxLimit = 131072;

        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            Console.WriteLine(String.Format("Before Constructor: MilliS {0}", sw.ElapsedMilliseconds));
            IOneToManyMapper map = new OneToManyMapper1();

            Console.WriteLine(String.Format("Start Create list: MilliS {0}", sw.ElapsedMilliseconds));
            Random rand = new Random();

            for (int parent = 1; parent < maxLimit; parent++)
            {
                int nextParent = rand.Next(minLimit, maxLimit);
                int nextChild = rand.Next(minLimit, maxLimit);

                map.Add(nextParent, nextChild);
            }

            Console.WriteLine(String.Format("Start Get Children: MilliS {0}", sw.ElapsedMilliseconds));
            for (int parent = 1; parent < maxLimit; parent++)
            {
                var x = map.GetChildren(parent);
                parent = parent + 5;
            }

            Console.WriteLine(String.Format("Start Get Parent: MilliS {0}", sw.ElapsedMilliseconds));
            for (int parent = 1; parent < maxLimit / 10000; parent++)
            {
                var x = map.GetParent(parent);
                parent = parent + 1;
            }

            Console.WriteLine(String.Format("Start Remove Child: MilliS {0}", sw.ElapsedMilliseconds));
            for (int parent = 1; parent < maxLimit / 10000; parent++)
            {
                map.RemoveChild(parent);
                parent = parent + 1;
            }

            Console.WriteLine(String.Format("Start Remove Parent: MilliS {0}", sw.ElapsedMilliseconds));
            for (int parent = 1; parent < maxLimit / 10000; parent++)
            {
                map.RemoveParent(parent);
                parent = parent + 1;
            }
            
            Console.WriteLine(String.Format("End: MilliS {0}", sw.ElapsedMilliseconds));
            Console.ReadLine();
        }
    }
}
