﻿using Mapper.Interfaces;
using Mapper47;
using System;
using System.Collections.Generic;

namespace Mapper.Mapper
{
    public class OneToManyMapper5 : IOneToManyMapper
    {
        private SortedList<int, int> Relations { get; set; }


        private int _minRange = Program.minLimit;
        private int _maxRange = Program.maxLimit;

        public OneToManyMapper5()
        {
            //child, parent
            Relations = new SortedList<int, int>();
        }

        public void Add(int parent, int child)
        {
            if ((parent > _maxRange) || (child > _maxRange) || (parent < _minRange) || (child < _minRange))
                throw new OverflowException(String.Format("Identifiers out of range, must be between {0} and {1}", _minRange, _maxRange));

            //if alread exists do nothing, just one child allowed
            if (!Relations.ContainsKey(child))
                Relations.Add(child, parent);
        }

        public IEnumerable<int> GetChildren(int parent)
        {
            List<int> result = new List<int>();
            foreach (var x in Relations)
            {
                if (x.Value == parent)
                {
                    result.Add(x.Key);
                }
            }
            return result;
        }

        public int GetParent(int child)
        {
            if (Relations.ContainsKey(child))
            {
                int index = Relations.IndexOfKey(child);
                return Relations.Keys[index];
            }

            return 0;
        }

        public void RemoveChild(int child)
        {
            if (Relations.ContainsKey(child))
                Relations.Remove(child);
        }

        public void RemoveParent(int parent)
        {
            if (Relations.ContainsValue(parent))
            {
                int index = Relations.IndexOfValue(parent);
                Relations.RemoveAt(index);
            }
        }
        //{
        //    foreach (var x in Relations)
        //    {
        //        int index = Relations.IndexOfValue(parent);
        //        if (x.Value == parent)
        //        {
        //            result.Add(x.Key);
        //        }
        //    }


    }
}
