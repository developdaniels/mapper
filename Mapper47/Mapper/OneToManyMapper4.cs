﻿using Mapper.Interfaces;
using Mapper47;
using System;
using System.Collections.Generic;

namespace Mapper.Mapper
{
    public class OneToManyMapper4 : IOneToManyMapper
    {
        private SortedList<int, SortedSet<int>> Relations { get; set; }


        private int _minRange = Program.minLimit;
        private int _maxRange = Program.maxLimit;

        public OneToManyMapper4()
        {
            Relations = new SortedList<int, SortedSet<int>>();
        }

        public void Add(int parent, int child)
        {
            if ((parent > _maxRange) || (child > _maxRange) || (parent < _minRange) || (child < _minRange))
                throw new OverflowException(String.Format("Identifiers out of range, must be between {0} and {1}", _minRange, _maxRange));

            if (Relations.ContainsKey(parent))
                Relations[parent].Add(child);
            else
                Relations.Add(parent, new SortedSet<int>() { child });
        }

        public IEnumerable<int> GetChildren(int parent)
        {
            if (Relations.ContainsKey(parent))
                return Relations[parent];
            else
                return new HashSet<int>();
        }

        public int GetParent(int child)
        {
            foreach (var relation in Relations)
            {
                if (relation.Value.Contains(child))
                    return relation.Key;
            }

            return 0;
        }

        public void RemoveChild(int child)
        {
            foreach (var relation in Relations)
            {
                if (relation.Value.Contains(child))
                    relation.Value.Remove(child);
            }
        }

        public void RemoveParent(int parent)
        {
            if (Relations.ContainsKey(parent))
                Relations.Remove(parent);
        }
    }
}
